console.log("hello")


// While Loop

/*
	-takes in an expression/condition
	-expressions are any unit of code that can be evaluated to a value 
	- if the evaluation of the condition is true, the statements will be executed
	-a loop will iterate a certain number of times until the expression is/is not met.
	- "iteration" is the term given to the repition of the statement/s

	SYNTAX:
		while(expressions/condition){
			statement/s;
		}
*/
let count = 5;

while (count !== 0){
	console.log("While loop result: "+ count);

	count--;
};



count = 0;

while (count <= 10){
	console.log("While loop result: "+ count);

	count++;
};

// Do-while loop

/*
	-a do-while loops works a lut like the while loop. but, unlike while loop, do-while loop guarantee that the code will be executed atleast once.

	SYNTAX:
		do{
			statments
		}while(expression/condition)


*/

/*
	-Number function works similarly to the "parseInt" function
	-both differ significantly in terms of process they undertake in converting information into a number data type
	- How do-while loop works:
		1. the statements in "do" block executes once
		2. the message in the statements will be executed
		3. after executing once, the while statement will evaluate wether to run the iteration of the loop or not based on the expression(if the numbers is less than 10)
		4. if the condition is true, an iteration will be done
		5. if the condition is false, the loop will stop


*/
// let number = Number(prompt("Give me a number"));

// do{
// 	console.log("Do-while Loop: "+ number);

// 	number+=1;
// }while (number < 10);


// for Loop

/*
	- the most flexible looping compared to do-while loop and while loops.
	1. initialization - tracks the progression of the loop
	2. condition/expression - will be evaluated which will determine wether the loop will run one more time
	3. finalExpression - indicates how to advance the loop


	Stages of For Loop
	- will initialize a variable "count" that has the value of 0;
	- the condition/expression that is to be assessed is if the value  of "count" is less than or equal to 20
	-perform the statements should the condition/expression returns true
	- increment the value of count

*/
for(let count = 0; count<=20; count++){
	console.log("For loop: " + count)
}

// using Strings
/*
	characters in a string may be counted using the .length property

	-strings are special comapred to other data types in that it hass access to functions and other pieces of infomation another data type might no have

*/

let myString = "alex"
console.log(myString);
for(let x=0; x < myString.length; x++){
	console.log(myString[x]);
};



let myName = "JOsHua"
for (let x = 0; x<myName.length; x++) {
	if (
		myName[x].toLowerCase() ==="a" ||
	    myName[x].toLowerCase() ==="e" ||
	    myName[x].toLowerCase() === "i"||
	    myName[x].toLowerCase() ==="o" || 
	    myName[x].toLowerCase() ==="u"
	    ) {	

		console.log(3)
	}else console.log(myName[x].toLowerCase())
}



// Continue and Break Statements

for(let count = 0; count <=20; count++){
	if (count % 2 === 0){
		continue;
	};
	console.log("Continue and Break: " + count);

	if (count>10){
		break;
	};
};


let name ="alexandro";

for (let i = 0; i <name.length; i++){
	// console.log(name[i]) will display "a" and "d" in the string
	if (name[i] === "a"){
		continue;
	};
	// console.log(name[i]) will not display "a" in the string
	if (name[i] === "d"){
		break;
	}
	console.log(name[i])
	
}


